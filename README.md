# Nature Journal Downloader
On GitLab as [https://gitlab.com/kev-m/nature-journal-downloader](https://gitlab.com/kev-m/nature-journal-downloader).

## Python script to compile a PDF of the free content from the journal "Nature"

This script downloads free content from [Nature.com](https://nature.com/nature) as individual articles 
and compiles a single output PDF from the multiple free articles per issue, suitable for offline reading.

**Note:** This script only downloads the FREE content. It does not include the academic articles that 
are only available in the full journal.

### Usage
The script provides its own help:
```bash
python3 nature_journal_dl.py -h
```
#### Download the Current Issue
To download the current issue to the default output folder `./output_pdfs`
```bash
python3 nature_journal_dl.py
```
#### Download a Specific Issue
To download a specific issue, specify the volume and the issue:
```bash
python3 nature_journal_dl.py --volume X --issue Y 
```

The full list of volumes and issues is available from Nature:
[https://www.nature.com/nature/volumes](https://www.nature.com/nature/volumes)

#### Specifying the alternate directories
By default, the script stores the final compiled PDF in `./output_pdfs` and the intermediate content in `./temp/pdf{issue_number}`.

These default locations can be overridden from the command-line with the following parameters:
```bash
python3 nature_journal_dl.py   --temp_dir TEMP_DIR  --pdf_dir PDF_DIR
```

TEMP_DIR is the temporary directory for the intermediate downloaded files (html and individual article PDF files).

PDF_DIR is the output directory, where the final compiled PDF is written (e.g. Nature-Volume580_Issue7805.pdf)


#### Caching
The script caches downloaded files and does not re-download them if they exist. If however, a file is not
correctly downloaded or is otherwise corrupted, then you can force a download by:

```bash
python3 nature_journal_dl.py   --cached n
```

# Installation and Other Details

## Requirements
The script requires Python 3.

Install required packages with:

```bash
pip3 install -r requirements.txt
```

### Running in a Virtual Environment:

```bash
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt
python3 nature_journal_dl.py
```
