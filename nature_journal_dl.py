#!/usr/bin/env python3
#
# Download the free content of the specified Nature Journal and compile it
# into a single PDF document.
#
# optional arguments:
#   --volume VOLUME      Volume number
#   --issue ISSUE        Issue number
# Note: If used, BOTH the volume and issue number must be specified.
#   --cached CACHED      Use cached html? y/n
#   --temp_dir TEMP_DIR  Temporary directory for downloaded files
#   --pdf_dir PDF_DIR    Output directory, where final output PDF is written

# Logging
import logging

# Argument parsing
import argparse

# For file existence checking, etc
import os

# Further processing of issue
import nature_dl.download_content as download_content

# Reforming PDF
import nature_dl.parse_page_info as parse_page_info

# Generate the final compiled PDF
import nature_dl.generate_issue as generate_issue

def  extract_volume_issue_from_url(url):
    # url=https://www.nature.com/nature/volumes/578/issues/7795
    parts = url.partition("/issues/")
    logging.debug("parts=%s", parts)
    issue = parts[2]
    volume = parts[0].split("/")[-1]
    logging.info("volume=%s, issue=%s", volume, issue)
    return volume, issue

def create_directory(directory_name):
    logging.debug("Creating directory %s", directory_name)
    try:
        os.makedirs(directory_name)
    except FileExistsError:
        logging.debug("Directory already exists, continuing...", directory_name)

def get_and_create_pdf_download_dir(issue, temp_pdf_directory='temp/pdf'):
    pdf_directory = temp_pdf_directory+issue
    logging.debug("Creating PDF output directory %s", pdf_directory)
    return pdf_directory

# Fetch the HTML from the "current issue" URL, extract volume/issue numbers and return the volume/issue details and downloaded content
def generate_current_issue(temp_dir, pdf_output_dir, cached=True):
    logging.debug("Downloading information for current issue")

    final_url = download_content.get_final_url("https://www.nature.com/nature/current-issue")
    volume, issue = extract_volume_issue_from_url(final_url)

    return generate_specified_issue(volume, issue, temp_dir, pdf_output_dir, cached)

# The main workhorse function. It fetches the main HTML file for an issue, extracts the articles, 
# downloads the PDFs of the articles, processes the PDFs to compile the list of unique 
# pages and finally compiles a single PDF output containing the unique pages.
def generate_specified_issue(volume, issue, temp_dir, pdf_output_dir, cached=True):
    logging.info("Processing Nature: Volume %s, Issue %s", volume, issue)
    create_directory(pdf_output_dir)
    temp_pdf_directory = get_and_create_pdf_download_dir(issue, temp_dir+'/pdf')
    create_directory(temp_pdf_directory)

    # Download the HTML for the issue from Nature.com
    issue_html_file = download_content.download_issue_html_by_volume_and_issue(volume, issue, cached)

    # Extract the articles in the issue
    articles = parse_page_info.extract_issue_articles(issue_html_file)
    
    # Download the article PDFs
    pdf_files, failures = download_content.fetch_article_pdfs('https://www.nature.com/magazine-assets/', temp_pdf_directory, articles)

    # Extract the unique pages from all downloaded article PDFs (since some pages contain more than 1 article)
    file_pages_dictionary = parse_page_info.extract_issue_info_from_pdfs(pdf_files)
    
    # Compile the output PDF for the issue from the unique pages
    pdf_output_name = pdf_output_dir+'/Nature-Volume'+volume+'_Issue'+issue+'.pdf'
    logging.info("Compiling output: '%s'", pdf_output_name)
    generate_issue.generate_single_issue_pdf(pdf_output_name, file_pages_dictionary)

    # Return success (if no exceptions were generated, etc)
    return 0

if __name__ == '__main__':
    FORMAT = '%(message)s'
    logging.basicConfig(format=FORMAT,level=logging.INFO)
    #logging.basicConfig(format=FORMAT, level=logging.DEBUG)

    parser = argparse.ArgumentParser(description='Generage nature issue PDF from online downloadable resources')
    parser.add_argument('--volume', type=str, help='Volume number', default=0)
    parser.add_argument('--issue', type=str, help='Issue number', default=0)
    parser.add_argument('--cached', type=str, help='Use cached html? y/n', default="Y")
    parser.add_argument('--temp_dir', type=str, help='Temporary directory for downloaded files', default='temp')
    parser.add_argument('--pdf_dir', type=str, help='Output directory, where final output PDF is written', default='output_pdfs')

    args = parser.parse_args()

    cached = (args.cached.upper() == "Y")

    if (args.volume != args.issue) and args.issue == 0:
        print("Both --volume and --issue are required together")
        exit(1)

    if args.issue == 0:
        exit(generate_current_issue(args.temp_dir, args.pdf_dir, cached))

    exit(generate_specified_issue(args.volume, args.issue, args.temp_dir, args.pdf_dir, cached))

