# Download the issue HTML and the article PDFs from Nature.com

# Basic stuff
import os
import sys
# Logging
import logging

# URL downloading, etc.
import requests

# Fetching issue HTML and PDFs from web servers
import urllib.request

# CODEC needed for Windows
import codecs

# To Sort pages
from collections import OrderedDict

# Download the issue from:
#   https://www.nature.com/nature/volumes/${volume}/issues/${issue}
def download_issue_html_by_volume_and_issue(volume, issue, cached=True, temp_dir='temp'):
    issue_html_file = temp_dir+"/issue"+issue+".html"
    if os.path.isfile(issue_html_file) == True:
        if cached == True:
            logging.debug("Cached == True, using issue_html_file %s", issue_html_file)
            return issue_html_file
        logging.debug("Cached == False, Deleting issue_html_file %s", issue_html_file)
        os.remove(issue_html_file)

    url = "https://www.nature.com/nature/volumes/"+volume+"/issues/"+issue
    logging.info("Downloading content from URL: %s", url)
    r = requests.get(url, allow_redirects=True)

    logging.debug("Content encoding is: %s", r.encoding)
    logging.debug("Saving to issue_html_file %s", issue_html_file)
    try:
        # Use the appropriate CODEC
        with codecs.open(issue_html_file, "w", "UTF-8") as temp:
            temp.write(r.text)
    except IOError as e:
        logging.error("Error writing content to file", e)
        return None

    return issue_html_file

# Given an input URL, find the final destination URL (Nature.com includes 
# authorisation and other redirects)
def get_final_url(url):
    # Nature requires dummy authorisation, so enable redirects to let Python sort it all out
    header = requests.head(url, allow_redirects=True)
    url = header.url

    # e.g. url=https://www.nature.com/nature/volumes/578/issues/7795
    logging.debug("url=%s", url)
    
    return url

# Given: A base URL, an output directory and a list of article IDs,
# download the PDFs of the articles from the URL, assuming:
# The origin URL is base_url+<article>/<article>.pdf, e.g.:
# https://www.nature.com/magazine-assets/<article>/<article>.pdf
# Return: List of PDF filenames (full path)
def fetch_article_pdfs(base_url, pdf_directory, articles):
    pdf_files = []
    failures = []

    for article in articles:
        try:
            filename = article+'.pdf'
            pdf_file = pdf_directory+'/'+ filename
            if os.path.isfile(pdf_file) == False:
                logging.info("Downloading %s", pdf_file)
                URL = base_url + article +'/'+filename
                result = urllib.request.urlretrieve(URL, filename=pdf_file)

            pdf_files.append(pdf_file)
        except Exception as e:
            print (e)
            continue

    return pdf_files, failures
