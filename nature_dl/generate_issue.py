from PyPDF2 import PdfFileWriter, PdfFileReader

# Given an output filename and a dictionary, generate the concatenated output from the dictionary.
# Where the dictionary consists of "filename -> list of page indices" for each input PDF file, e.g.:
#   {"file1":[0], "file2":[0,1], "file3":[1], ...}
# then the output conists of page 1 from file1, pages 1 & 2 from file2, page 2 from file3, ...
def generate_single_issue_pdf(pdf_output_name, file_page_dictionary):
    output = PdfFileWriter()

    # TODO: Add front page
    # TODO: Add table of contents

    # Iterate over all pdf_files in dictionary of unique pages
    pdf_files = [*file_page_dictionary.keys()]
    for pdf_file in pdf_files:
        input_pdf = PdfFileReader(open(pdf_file, 'rb'))
        page_indices = file_page_dictionary[pdf_file]
        for page_index in page_indices:
            page = input_pdf.getPage(page_index)
            output.addPage(page)

    with open(pdf_output_name, 'wb') as f:
        output.write(f)
