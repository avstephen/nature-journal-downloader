# Extract the Nature issue page numbers from the PDF for an individual article: Given a database and a list of PDFs, extract the page numbers of all the content.
# The problem is that a single page in Nature can contain content from multiple articles. Therefore, the individual article PDFs can contain duplicate pages.
# The purpose here is to build a dictionary of "pages" per "PDF file" so that a final output of unique, sequential pages can be created.
#
# e.g. for the following files containing the given pages, the compiled PDF should consist of only F2, F4 and pages 2 & 3 from F5
#  File: Pages
#  F1: 8
#  F2: 8,9
#  F3: 9,10
#  F4: 10,11,12
#  F5: 12,13,14

import logging
import sys

# Parsing HTML
from bs4 import BeautifulSoup

# Need to specifiy the CODEC for Windows / codepage
import codecs

from collections import OrderedDict
from PyPDF2 import PdfFileReader

# Extract the article id from the article <a href..>
# e.g. "d41586-019-02491-x" from "/articles/d41586-019-02491-x"
def add_to_articles(articles, entry):
    href = entry.get('href')
    leader = '/articles/'
    # Add the 's' since only downloadable content starts with '/articles/s*'
    if href.find(leader) > -1:
        key = href[len(leader):]
        logging.debug("key=%s",key)
        articles.append(key)

# Find the article <a href..> entries in the issue HTML using the following patterns
# In the article:
# <a href="/magazine-assets/d41586-019-02478-8/d41586-019-02478-8.pdf" target="_blank" rel="noreferrer noopener" data-track="download" data-track-label="PDF download">PDF version</a>
# In the "master":
# <a href="/articles/s41586-019-1480-0" itemprop="url" data-track="click" data-track-action="view article" data-track-label="link">                             Publisher Correction: Intercellular interaction dictates cancer cell ferroptosis via NF2&acirc;&#128;&#147;YAP signalling</a>
def is_article(tag):
    if (tag.has_attr('data-track-action') and tag['data-track-action'] == 'view article'):
        href = tag.get('href')
        if href.find('/articles/d') > -1:
            return True
    return False

# Parse the issue HTML for all "article" <a href..>s and extract article identifiers
def extract_issue_articles(html_path):
    articles = []
	# Specify the encoding
    with codecs.open(html_path, "r", "UTF-8") as fp:
        soup = BeautifulSoup(fp, "html.parser")

        for each in soup.find_all(is_article):
            add_to_articles(articles, each)

    return articles

# Given: The list of pdf_files for this issue
# Return: the dictionary of unique pages, keyed by input filename
def extract_issue_info_from_pdfs(pdf_files):
    # PDF/Page dictionary
    pdf_pages = OrderedDict()

    last_page = 0
    # Given the list of articles, get the PDF information
    for pdf_file in pdf_files:
        logging.debug("Processing PDF %s", pdf_file)
        # Define empty page array
        page_array = []
        # It can happen that first_page is 0 when the PDF has no /PageLabel object
        first_page, number_of_pages = get_pdf_page_info(pdf_file, last_page)
        last_page = first_page + number_of_pages - 1
        for page in range(number_of_pages):
            page_array.append(first_page+page)
        # Add the array to the dictionary
        pdf_pages[pdf_file]=page_array

    # Extract the unique pages spanned by all the PDFs
    file_pages_dictionary = extract_sequential_pages(pdf_pages)

    return file_pages_dictionary

# Algorithm: Sequentially extract the pages as you find them
# Given a page-sorted dictionary that contains {"filename", [page numbers]}, extract the sequential pages as you find them.
# Return: dictionary of unique pages, keyed by input filename,
#   e.g. {filename1: [pg0, pg1, pg2], ...}
def extract_sequential_pages(unsorted_dictionary):
    # The pages/keys are not always sorted
    sorted_dictionary = sort_page_dictionary(unsorted_dictionary)
    files = [*sorted_dictionary.keys()]

    results = {}
    current_index = 0
    current_page = 0
    while current_index < len(files):
        file_index, next_pages, current_page = get_pages_within_pdf_entry(sorted_dictionary, files, current_index, current_page)
        if len(next_pages) > 0:
            logging.debug("Compiling file: %s, pages: %s", files[file_index], next_pages)
            results[files[file_index]] = next_pages

            # "corrections" appear at the end of an article, with the same page number as the next article.
            # The duplicate "corrections" page overwrites the desired start page of the next article.
            # Solution: Try and replace the "previous final page" with the "next first page", if it exists?
            # "next_pages" is a set of indices.

            # (a) If the "next_pages[0] is 1" and (b) the previous "next_pages" length is > 1,
            # then (c) prepend 0 to next_pages and (d) drop the last page from the previous one.

            # (a) If the "next_pages[0] is 1"
            if (next_pages[0] == 1) & (file_index >= 1):
                try: # Sometimes there's a Key Error, just catch and skip
                    prev_pages = results[files[file_index-1]]
                except KeyError as e:
                    logging.error("Key error looking for index %d, file %s", file_index, files[file_index-1])
                    break
                # (b) the previous "next_pages" length is > 1
                if (len(prev_pages) > 1):
                    # (c) prepend 0 to next_pages
                    next_pages[:0] = [0]
                    logging.debug("Testing next_pages: %s", next_pages)
                    # (d) drop the last page from the previous one.
                    prev_pages.pop()
                    results[files[file_index-1]] = prev_pages
                    logging.debug("Testing prev_pages: %s", prev_pages)

        current_index = file_index + 1

    logging.debug("No more files")

    return results

# Return the page number from the PDF file data
def extract_first_page_number_from_pdf(page):
    obj = page.getObject()
    page_number = obj['/St']
    return page_number


# Given a dictionary of: {'file1': [..pages..], 'file2': [..pages..],  ...}, e.g.
#  {'file-1.pdf': [149], 'file-2.pdf': [150], 'file-3.pdf': [151], 'file-4.pdf': [283, 284, 285], 'file-5.pdf': [163, 164, 165, 166]}
# Return a sorted dictionary where the pages are in increasing order, e.g.:
#  {'file-1.pdf': [149], 'file-2.pdf': [150], 'file-3.pdf': [151], 'file-5.pdf': [163, 164, 165, 166], 'file-4.pdf': [283, 284, 285]}
def sort_page_dictionary(unsorted_dictionary):
    logging.debug("Un-sorted: %s", unsorted_dictionary)
    sorted_dictionary = OrderedDict(sorted(unsorted_dictionary.items(), key=lambda t: t[1]))
    logging.debug("Sorted: %s", sorted_dictionary)

    return sorted_dictionary

# Determine the page number of the first page of the PDF and number of pages in the PDF
# Parameters:
#  path to PDF
#  page number of the last page found
# Returns: first page number of this PDF, number of pages
#
# Sometimes an article PDF does not have actual page numbers in its index.
# Solution: Guess the first_page by using the previous 'last_page'
# As last_page+number_of_pages
def get_pdf_page_info(path, last_page):
    with open(path, 'rb') as f:
        pdf = PdfFileReader(f)
        info = pdf.getDocumentInfo()
        number_of_pages = pdf.getNumPages()

        logging.debug("Number of pages = %d", number_of_pages)

        try:
            page_label_types = pdf.trailer["/Root"]["/PageLabels"]["/Nums"]
            first_page = extract_first_page_number_from_pdf(page_label_types[1])
            logging.debug("First page is number %d", first_page)
            return first_page, number_of_pages
        except:
            logging.warning("No /PageLabel object for %s, making a guess for the page number", path)
            return last_page+number_of_pages, number_of_pages

# Determine the current file index and the list of pages it provides
# - dictionary consists of {"filename", [page numbers]}
# - files consists of an array of PDF filenames
# - current_index is the index of the current filename in the "files" array
# - current_page is the current page that has been found, maybe be 0 (unknown)
# Return:
#  - file_index, the index of the files entry tried
#  - pages, the array of pages that this file provides
#  - next_page, the next page number to search for
def get_pages_within_pdf_entry(dictionary, files, current_index, current_page):
    this_pages = []
    next_page = current_page

    # Search for the next page (at least current_page+1, but maybe higher)
    next_pdf = files[current_index]
    next_pdf_pages = dictionary[next_pdf]

    logging.debug("Page: %s, Checking %s, with pages: %s", current_page, next_pdf, next_pdf_pages)

    page_index = 0
    for page in next_pdf_pages:
        if page >= next_page:
            this_pages.append(page_index)
            next_page = page+1
            logging.debug("Found page: %s, looking for page: %s", page, next_page)

        page_index+=1

    return current_index, this_pages, next_page
    
